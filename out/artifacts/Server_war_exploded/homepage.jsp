<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="json" uri="http://java.sun.com/jsp/jstl/xml" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <title>E-Banking</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
</head>
<body>
<%
    String acc_no = request.getParameter("acc_no");
    if(acc_no != null){
%>
<script type="text/javascript">
    $(document).ready(function() {
        $('#logout').click(function () {
            window.location = "index.jsp";
        });
        $("#account-summary-menu").click(function(){
            /*document.getElementById("account-summary-menu").style.marginRight= "0px";
             document.getElementById("account-summary-menu").style.borderTopRightRadius= "0px";
             document.getElementById("account-summary-menu").style.borderBottomRightRadius= "0px";*/
            document.getElementById("content-name").textContent = "Account Summary";
            $("#account-summary").show();
            $("#deposit").hide();
            $("#withdraw").hide();
            $("#add-beneficiary").hide();
            $("#transfer").hide();
            $("#mini-statement").hide();
        });
        $("#deposit-menu").click(function(){
            document.getElementById("content-name").textContent = "Deposit";
            $("#account-summary").hide();
            $("#deposit").show();
            $("#withdraw").hide();
            $("#add-beneficiary").hide();
            $("#transfer").hide();
            $("#mini-statement").hide();
        });
        $("#withdraw-menu").click(function(){
            document.getElementById("content-name").textContent = "Withdraw";
            $("#account-summary").hide();
            $("#deposit").hide();
            $("#withdraw").show();
            $("#add-beneficiary").hide();
            $("#transfer").hide();
            $("#mini-statement").hide();
        });
        $("#add-beneficiary-menu").click(function(){
            document.getElementById("content-name").textContent = "Add Beneficiary";
            $("#account-summary").hide();
            $("#deposit").hide();
            $("#withdraw").hide();
            $("#add-beneficiary").show();
            $("#transfer").hide();
            $("#mini-statement").hide();
        });
        $("#transfer-menu").click(function(){
            document.getElementById("content-name").textContent = "Fund Transfer";
            $("#account-summary").hide();
            $("#deposit").hide();
            $("#withdraw").hide();
            $("#add-beneficiary").hide();
            $("#transfer").show();
            $("#mini-statement").hide();
        });
        $("#mini-statement-menu").click(function(){
            document.getElementById("content-name").textContent = "Transaction history";
            $("#account-summary").hide();
            $("#deposit").hide();
            $("#withdraw").hide();
            $("#add-beneficiary").hide();
            $("#transfer").hide();
            $("#mini-statement").show();
        });
        $('#transaction_type').change(function(){
            var selected = $(this).children(":selected").text();
            switch (selected) {
                case "all transactions":
                    console.log("all transactions")
                    /*$("#myform").attr('action', 'mysql.html');
                     alert("Form Action is Changed to 'mysql.html'\n Press Submit to Confirm");*/
                    $("#table1 tr").remove();
                    settable(1);
                    break;
                case "cheques":
                    $("#table1 tr").remove();
                    settable(2);
                    break;
                case "withdraw and deposits":
                    $("#table1 tr").remove();
                    settable(3);
                    break;
                case "inbank transactions":
                    $("#table1 tr").remove();
                    settable(4);
                    break;
                case "other bank transactions":
                    $("#table1 tr").remove();
                    settable(5);
                    break;
                default:
                    $("#myform").attr('action', '#');
            }
            function settable(transaction_type){
                $.ajax({
                    type: "post",
                    url: "com.bank.webclient.TransactionHistoryServlet",
                    data: "acc_no=<%= acc_no%>&transaction_type="+transaction_type,
                    success: function(msg){
                        console.log(msg);
                        var tr;
                        tr = $('<tr/>');
                        tr.append("<th>Transaction type</th>");
                        tr.append("<th>Amount</th>");
                        tr.append("<th>Time</th>");
                        $('#table1').append(tr);
                        for (var i = 0; i < msg.length; i++) {
                            tr = $('<tr/>');
                            tr.append("<td>" + msg[i].type + "</td>");
                            tr.append("<td>" + "₹ " + msg[i].amount + "</td>");
                            tr.append("<td>" + msg[i].timestamp + "</td>");
                            $('#table1').append(tr);
                        }
                    },
                    error : function(errorThrown) {
                        alert(errorThrown);
                    }
                });
            }
        });
    });
</script>
<script type="text/javascript">
    console.log("script working")
            $.ajax({
                type: "post",
                url: "com.bank.webclient.GetAccountDetailsServlet",
                data: "acc_no=<%= acc_no%>",
                success: function(msg){
                    document.getElementById("name").innerHTML = "Welcome "+msg.name;
                    document.getElementById("acc_no").innerHTML = "Account number: "+msg.acc_no;
                    document.getElementById("balance").innerHTML = "Total available balance  ₹"+msg.balance;
                    document.getElementById("type").innerHTML = msg.type+" Account";

                    var name = msg.name;
                    var balance = msg.balance;
                    var type = msg.type;
                },
                error : function(errorThrown) {
                    alert(errorThrown);
                }
            });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#inbank-submit').click(function () {
            console.log("inbank submit clicked");
            if($('#inbank-accno').val()){
                if($('#inbank-accno').val() == <%= acc_no%>){
                    document.getElementById("message").style.color = "red";
                    document.getElementById("message").textContent = "You cannot add your current account as beneficiary";
                }
                else {
                    $.ajax({
                        type: "post",
                        url: "com.bank.webclient.InbankBeneficiaryServlet",
                        data: "acc_no=<%= acc_no%>&to_acc_no="+$('#inbank-accno').val(),
                        success: function(msg){
                            $('#inbank').hide();
                            document.getElementById("inbank-beneficiary-success").style.display = "inline-block";
                            document.getElementById("message").innerHTML = "Successfull";
                        },
                        error : function(errorThrown) {
                            alert(errorThrown);
                        }
                    });
                }
            }
            else{
                document.getElementById("add-beneficiary-message").style.color = "red";
                document.getElementById("add-beneficiary-message").textContent = "please enter the account number.";
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#otherbank-submit').click(function () {
            console.log("inbank submit clicked");
            if($('#otherbank-accno').val()){

                    $.ajax({
                        type: "post",
                        url: "com.bank.webclient.OtherbankBeneficiaryServlet",
                        data: "from_acc_no=<%= acc_no%>&acc_no="+$('#otherbank-accno').val()+"&name="+$('#otherbank-name').val()+"&bank_name="
                                +$('#otherbank-bankname').val()+"&branch_name="+$('#otherbank-branch').val()+"&ifsc="+$('#otherbank-ifsc').val(),
                        success: function(msg){
                            $('#inbank').hide();
                            document.getElementById("otherbank-beneficiary-success").style.display = "inline-block";
                        },
                        error : function(errorThrown) {
                            alert(errorThrown);
                        }
                    });
            }
            else{
                document.getElementById("add-beneficiary-message").style.color = "red";
                document.getElementById("add-beneficiary-message").textContent = "please enter the account number.";
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#deposit-submit').click(function () {
            console.log("deposit submit clicked");
            if($('#deposit-amount').val()){
                    $.ajax({
                        type: "post",
                        url: "com.bank.webclient.DepositServlet",
                        data: "acc_no=<%= acc_no%>&deposit_amount="+$('#deposit-amount').val(),
                        success: function(msg){
                            $('#deposit-form').hide();
                            document.getElementById("deposit-success").style.display = "inline-block";
                        },
                        error : function(errorThrown) {
                            alert("errorThrown");
                        }
                    });
            }
            else{
                document.getElementsByClassName("message").style.color = "red";
                document.getElementsByClassName("message").textContent = "please enter the account number.";
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#withdraw-submit').click(function () {
            console.log("deposit submit clicked");
            if($('#withdraw-amount').val()){
                $.ajax({
                    type: "post",
                    url: "com.bank.webclient.WithdrawServlet",
                    data: "acc_no=<%= acc_no%>&withdraw_amount="+$('#withdraw-amount').val(),
                    success: function(msg){
                        $('#withdraw-form').hide();
                        document.getElementById("withdraw-success").style.display = 'inline-block';
                    },
                    error : function(errorThrown) {
                        alert("errorThrown");
                    }
                });
            }
            else{
                document.getElementsByClassName("message").style.color = "red";
                document.getElementsByClassName("message").textContent = "please enter the account number.";
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var flag = 0;
        var beneficiary_list;
        var selected =0;
        $('#beneficiary-list').click(function(){
            if(flag == 0){
                flag = 1;
                $.ajax({
                    type: "post",
                    url: "com.bank.webclient.GetBeneficiaryListServlet",
                    data: "acc_no=<%= acc_no%>",
                    success: function(msg){
                        beneficiary_list = msg;
                        console.log(msg);
                        var tr;
                        for (var i = 0; i < msg.length; i++) {
                            tr = $('<option/>');
                            tr.append(msg[i].to_account);
                            $('#beneficiary-list').append(tr);
                        }
                    },
                    error : function(errorThrown) {
                        alert(errorThrown);
                    }
                });
            }
        });
        $('#beneficiary-list').change(function(){
            selected = $(this).children(":selected").index();
            document.getElementById("to_account").textContent = "Selected Account  :"+beneficiary_list[selected-1].to_account;


        });
        $('#transfer-submit').click(function () {
            console.log("transfer submit clicked");
            x = beneficiary_list[selected-1].to_account;
            var to_account_no;
            if(beneficiary_list[selected-1].type == 1){
                to_account_no = x;
            }else{
                to_account_no = x.substr(0,x.indexOf(' '));
            }
            if($('#transfer-amount').val()){

                $.ajax({
                    type: "post",
                    url: "com.bank.webclient.FundtransferServlet",
                    data: "acc_no=<%= acc_no%>&transfer_amount="+$('#transfer-amount').val()+"&to_acc_no="+ to_account_no +"&pair_id="+ beneficiary_list[selected-1].pair_id+"&type="+beneficiary_list[selected-1].type,
                    success: function(msg){
                        $('#transfer-form').hide();
                        document.getElementById("transfer-success").style.display = "inline-block";
                    },
                    error : function(errorThrown) {
                        alert("errorThrown");
                    }
                });
            }
            else{
                document.getElementsByClassName("message").style.color = "red";
                document.getElementsByClassName("message").textContent = "please enter the account number.";
            }
        });

    });
</script>


<div class="header">
    <div id="main-text"><b>Internet Banking Application</b></div>
    <div id="logout"><b>LOGOUT</b></div>
</div>

<div class="menu">
    <div class="menu-top-bar">
        <span>Accounts</span>
    </div>
    <div class="menu-item" id="account-summary-menu">
        <span>Account summary</span>
    </div>
    <div class="menu-item" id="deposit-menu">
        <span>Deposit</span>
    </div>
    <div class="menu-item" id="withdraw-menu">
        <span>Withdraw</span>
    </div>
    <div class="menu-item" id="add-beneficiary-menu">
        <span>Add beneficiary</span>
    </div>
    <div class="menu-item" id="transfer-menu">
        <span>Transfer</span>
    </div>
    <div class="menu-item" id="mini-statement-menu">
        <span>Mini statement</span>
    </div>

</div>

<div class="content">
    <div class="content-topbar">
        <span id ="content-name">Account summary</span>
    </div>
    <div id="account-summary">
        <div id="account-summary-main-area">
            <p id="name"></p>
            <p id="acc_no"></p>
            <p id="type"></p>
            <p id="balance"></p>
        </div>
    </div>
    <div id="deposit">
        <div id="deposit-form">
            <p>
                <label class="label">Deposit amount</label>
                <input type="text" id="deposit-amount">
            </p>
            <p class = "deposit-message"></p>
            <input type="submit" id="deposit-submit" value="Deposit">
        </div>
        <div class="successfull-entry" id="deposit-success">
            <p class="success-message"><b>Amount depositted successfully!!!</b></p>
        </div>
    </div>
    <div id="withdraw">
        <div id="withdraw-form">
            <p>
                <label class="label">Withdraw amount</label>
                <input type="text" id="withdraw-amount">
            </p>
            <p class = "withdraw-message"></p>
            <input type="submit" id="withdraw-submit" value="Withdraw">
        </div>
        <div class="successfull-entry" id="withdraw-success">
            <p class="success-message"><b>Amount withdrawn successfully!!!</b></p>
        </div>
    </div>
    <div id="add-beneficiary">
        <div id="beneficiary-block">
            <div id="inbank-block">
                <div id="inbank">
                    <p>
                        <label class="label">Account number</label>
                        <input type ="text" id="inbank-accno"/>
                    </p>
                    <p id = "add-beneficiary-message"></p>
                    <input type="submit" id="inbank-submit" value="Add beneficiary"/>

                </div>
                <div class="successfull-entry" id="inbank-beneficiary-success">
                    <p class="success-message"><b>Beneficiary added successfully!!!</b></p>
                </div>
            </div>
            <div id="otherbank-block">
                <div>
                    <p>
                        <label class="label">Account number</label>
                        <input type ="text" id="otherbank-accno"/>
                    </p>
                    <p>
                        <label class="label">Name</label>
                        <input type ="text" id="otherbank-name"/>
                    </p>
                    <p>
                        <label class="label">Bank</label>
                        <input type ="text" id="otherbank-bankname"/>
                    </p>
                    <p>
                        <label class="label">Branch</label>
                        <input type ="text" id="otherbank-branch"/>
                    </p>
                    <p>
                        <label class="label">IFSC</label>
                        <input type ="text" id="otherbank-ifsc"/>
                    </p>
                    <p id = "add-otherbank-beneficiary-message"></p>
                    <input type="submit" id="otherbank-submit" value="Add beneficiary"/>

                </div>
                <div class="successfull-entry" id="otherbank-beneficiary-success">
                    <p class="success-message"><b>Beneficiary added successfully!!!</b></p>
                </div>
            </div>
        </div>
    </div>
    <div id="transfer">
        <div id="transfer-form">
            <select  id = "beneficiary-list" name="beneficiary-list">
                <option value="select">---select---</option>
            </select>
            <p class="label" id="to_account"></p>
            <p>
                <label class="label">Transfer amount</label>
                <input type="text" id="transfer-amount">
            </p>
            <p class = "deposit-message"></p>
            <input type="submit" id="transfer-submit" value="Transfer">
        </div>
        <div class="successfull-entry" id="transfer-success">
            <p class="success-message"><b>Amount Transfered successfully!!!</b></p>
        </div>
    </div>
    <div id="mini-statement">
        <div id="mini-statement-table">
            <select  id = "transaction_type" name="transaction_type">
                <option value="select">---select---</option>
                <option value="all transactions">all transactions</option>
                <option value="cheques">cheques</option>
                <option value="withdraw and deposits">withdraw and deposits</option>
                <option value="inbank transactions">inbank transactions</option>
                <option value="other bank transactions">other bank transactions</option>
            </select>
            <table id="table1"></table>
        </div>
    </div>

</div>

<%}%>
</body>
</html>
