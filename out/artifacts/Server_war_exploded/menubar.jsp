<%--
  Created by IntelliJ IDEA.
  User: rathi-pt1424
  Date: 2/24/2017
  Time: 7:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="json" uri="http://java.sun.com/jsp/jstl/xml" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <title>E-Banking</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <title>Title</title>
</head>
<body>
<%
    String acc_no = request.getParameter("acc_no");
    if(acc_no != null){
%>
<script type="text/javascript">
    $(document).ready(function() {
        $("#account-summary-menu").click(function(){
            $("#account-summary-menu").addClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
            $("#withdraw-menu").removeClass("menu-item-selected");
            $("#add-beneficiary-menu").removeClass("menu-item-selected");
            $("#transfer-menu").removeClass("menu-item-selected");
            $("#mini-statement-menu").removeClass("menu-item-selected");
            parent.content.location = "content.jsp?acc_no=<%=acc_no%>&item_no=1";
        });
        $("#deposit-menu").click(function(){
            $("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").addClass("menu-item-selected");
            $("#withdraw-menu").removeClass("menu-item-selected");
            $("#add-beneficiary-menu").removeClass("menu-item-selected");
            $("#transfer-menu").removeClass("menu-item-selected");
            $("#mini-statement-menu").removeClass("menu-item-selected");
            parent.content.location = "content.jsp?acc_no=<%=acc_no%>&item_no=2";
        });
        $("#withdraw-menu").click(function(){
            $("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
            $("#withdraw-menu").addClass("menu-item-selected");
            $("#add-beneficiary-menu").removeClass("menu-item-selected");
            $("#transfer-menu").removeClass("menu-item-selected");
            $("#mini-statement-menu").removeClass("menu-item-selected");
            parent.content.location = "content.jsp?acc_no=<%=acc_no%>&item_no=3";
        });
        $("#add-beneficiary-menu").click(function(){
            $("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
            $("#withdraw-menu").removeClass("menu-item-selected");
            $("#add-beneficiary-menu").addClass("menu-item-selected");
            $("#transfer-menu").removeClass("menu-item-selected");
            $("#mini-statement-menu").removeClass("menu-item-selected");
            parent.content.location = "content.jsp?acc_no=<%=acc_no%>&item_no=4";
        });
        $("#transfer-menu").click(function(){
            $("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
            $("#withdraw-menu").removeClass("menu-item-selected");
            $("#add-beneficiary-menu").removeClass("menu-item-selected");
            $("#transfer-menu").addClass("menu-item-selected");
            $("#mini-statement-menu").removeClass("menu-item-selected");
            parent.content.location = "content.jsp?acc_no=<%=acc_no%>&item_no=5";
        });
        $("#mini-statement-menu").click(function(){
            $("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
            $("#withdraw-menu").removeClass("menu-item-selected");
            $("#add-beneficiary-menu").removeClass("menu-item-selected");
            $("#transfer-menu").removeClass("menu-item-selected");
            $("#mini-statement-menu").addClass("menu-item-selected");
            parent.content.location = "content.jsp?acc_no=<%=acc_no%>&item_no=6";
        });
    });
</script>

<div class="menu">
    <div class="menu-top-bar">
        <span>Accounts</span>
    </div>
    <div class="menu-item" id="account-summary-menu">
        <span>Account summary</span>
    </div>
    <div class="menu-item" id="deposit-menu">
        <span>Deposit</span>
    </div>
    <div class="menu-item" id="withdraw-menu">
        <span>Withdraw</span>
    </div>
    <div class="menu-item" id="add-beneficiary-menu">
        <span>Add beneficiary</span>
    </div>
    <div class="menu-item" id="transfer-menu">
        <span>Transfer</span>
    </div>
    <div class="menu-item" id="mini-statement-menu">
        <span>Mini statement</span>
    </div>

</div>
<%
    }%>
</body>
</html>
