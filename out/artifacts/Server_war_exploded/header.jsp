<%--
  Created by IntelliJ IDEA.
  User: rathi-pt1424
  Date: 2/24/2017
  Time: 7:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="json" uri="http://java.sun.com/jsp/jstl/xml" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <title>E-Banking</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <title>Title</title>
</head>
<body>

<%
    String acc_no = request.getParameter("acc_no");
    if(acc_no != null){
%>
<script type="text/javascript">
    $.ajax({
        type: "post",
        url: "com.bank.webclient.GetAccountDetailsServlet",
        data: "acc_no=<%=acc_no%>",
        success: function(msg){
            document.getElementById("welcome-text").innerHTML = "Welcome "+msg.name;
        },
        error : function(errorThrown) {
            alert(errorThrown);
        }
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#logout').click(function () {
            $.ajax({
                type: "post",
                url: "com.bank.webclient.LogoutServlet",
                success: function(msg){
                    parent.location.href="index.jsp";
                    console.log("logged out")
                },
                error : function(errorThrown) {
                    alert(errorThrown);
                }
            });

        });
    });
</script>
<div class="header">
    <div id="main-text"><b>Internet Banking Application</b></div>
    <div id="logout"><b>LOGOUT</b></div>
    <p id="welcome-text"></p>
</div>
<%
    }
%>
</body>
</html>
