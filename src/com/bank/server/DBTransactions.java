package com.bank.server;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.*;

/**
 * Created by rathi-pt1424 on 2/24/2017.
 */
public class DBTransactions {

    static ResultSet rs;
    static String s;
    static Statement st;
    static Connection con = null;

    public static void main(String[] args){

        System.out.println(get_beneficiary_list("2001"));

    }

    public static JSONArray get_beneficiary_list(String acc_no){

        JSONArray inbank_transaction_json = new JSONArray();

        String sql_inbank_pairs = "select * from inbank_acc_pairs where from_acc_no = "+acc_no;
        String sql_otherbank_pairs = "select accno_benid_pairs.a_b_id, beneficiary.acc_no, beneficiary.username, beneficiary.bank_name, beneficiary.branch, beneficiary.ifsc from accno_benid_pairs\n" +
                "inner join beneficiary on accno_benid_pairs.ben_id = beneficiary.ben_id where accno_benid_pairs.acc_no = "+acc_no;
        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            rs = st.executeQuery(sql_inbank_pairs);
            while(rs.next()){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("to_account", rs.getString(2));
                jsonObject.put("pair_id", rs.getInt(3));
                jsonObject.put("type", 1);
                inbank_transaction_json.put(jsonObject);
            }
            rs = st.executeQuery(sql_otherbank_pairs);
            while(rs.next()){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("to_account", rs.getString(2)+" "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(5));
                jsonObject.put("pair_id", rs.getInt(1));
                jsonObject.put("type", 2);
                inbank_transaction_json.put(jsonObject);
            }
            rs.close();
            st.close();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inbank_transaction_json;
    }

    public static boolean fund_transfer(int acc_no, int transfer_amount, int pair_id, int type, int to_acc_no){

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String sql_create_transaction_id = "insert into transaction_id(timestamp) values('"+timestamp+"')";
        String sql_get_transaction_id = "select x_id from transaction_id where timestamp='"+timestamp+"'";
        String sql_update_balance = "update users set balance = balance -"+transfer_amount+" where acc_no = "+acc_no;

        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            st.executeUpdate(sql_create_transaction_id);
            rs = st.executeQuery(sql_get_transaction_id);
            rs.next();
            int transaction_id = rs.getInt(1);
            st.executeUpdate("insert into all_transactions values("+transaction_id+"," + transfer_amount + ")");
            if(type == 1){
                st.executeUpdate("insert into inbank_transactions values(" + pair_id + "," + transaction_id + ")");
                st.executeUpdate("update users set balance = balance +"+transfer_amount+" where acc_no = "+to_acc_no);
            }else
                if(type == 2){
                    st.executeUpdate("insert into other_bank_transactions values(" + pair_id + ",'NEFT'," + rs.getInt(1) + ")");
                }
            st.executeUpdate(sql_update_balance);

            st.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return true;
    }


}
