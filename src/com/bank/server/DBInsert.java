package com.bank.server;

import java.sql.*;

/**
 * Created by rathi-pt1424 on 2/23/2017.
 */
public class DBInsert {

    static ResultSet rs;
    static String s;
    static Statement st;
    static Connection con = null;

    public static void main(String[] args){

        //add_inbank_beneficiary(2001,2002);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println(timestamp);
        //deposit_amount(2001, 3000);
        System.out.println(add_otherbank_beneficiary(2003, 1002,"anem", "bank","brank","SBIN620002"));
    }

    public static boolean add_inbank_beneficiary(int acc_no, int to_acc_no){
        String sql_inbank_acc_pairs = "insert into inbank_acc_pairs values(+"+acc_no+","+to_acc_no+")";
        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            rs = st.executeQuery("select * from inbank_acc_pairs where from_acc_no="+acc_no+" and to_acc_no="+to_acc_no);
            if(!rs.next()){
                st.executeUpdate(sql_inbank_acc_pairs);
                System.out.println("not null");
            }
            else{
                System.out.println("null");
                return false;
            }
            st.close();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean add_otherbank_beneficiary(int from_acc_no, int acc_no, String name, String bank_name, String bank_branch, String IFSC){
        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            rs = st.executeQuery("select ben_id from beneficiary where acc_no="+acc_no+" and ifsc='"+IFSC+"'");
            if(rs.next()){
                ResultSet rs2 = st.executeQuery("select * from accno_benid_pairs where ben_id ="+rs.getString(1));
                if(rs2.next()){
                    if(rs2.getString(1).equals(""+from_acc_no) && rs2.getString(2).equals(rs.getString(1)))
                        return false;
                    else
                        st.executeUpdate("insert into accno_benid_pairs values("+from_acc_no+","+rs2.getString(2)+")");
                }
                else{
                    st.executeUpdate("insert into accno_benid_pairs values("+from_acc_no+","+rs2.getString(2)+")");
                }
            } else{
                st.executeUpdate("insert into beneficiary(acc_no, username, ifsc, bank_name, branch) values("+acc_no+", '"+name+"', '"+IFSC+"', '"+bank_name+"', '"+bank_branch+"')");
                rs = st.executeQuery("select ben_id from beneficiary where acc_no="+acc_no+" and ifsc='"+IFSC+"'");
                rs.next();
                st.executeUpdate("insert into accno_benid_pairs values("+from_acc_no+","+rs.getString(1)+")");
            }
            st.close();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean deposit_amount(int acc_no, int deposit_amount){

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String sql_create_transaction_id = "insert into transaction_id(timestamp) values('"+timestamp+"')";
        String sql_get_transaction_id = "select x_id from transaction_id where timestamp='"+timestamp+"'";
        String sql_update_balance = "update users set balance = balance +"+deposit_amount+" where acc_no = "+acc_no;


        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            st.executeUpdate(sql_create_transaction_id);
            rs = st.executeQuery(sql_get_transaction_id);
            rs.next();
            int transaction_id= rs.getInt(1);
            st.executeUpdate("insert into all_transactions values(" + transaction_id + "," + deposit_amount + ")");
            st.executeUpdate("insert into withdrawanddeposit values("+acc_no+", 'deposit', "+transaction_id+")");
            st.executeUpdate(sql_update_balance);

            st.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean withdraw_amount(int acc_no, int withdraw_amount){

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String sql_create_transaction_id = "insert into transaction_id(timestamp) values('"+timestamp+"')";
        String sql_get_transaction_id = "select x_id from transaction_id where timestamp='"+timestamp+"'";
        String sql_update_balance = "update users set balance = balance -"+withdraw_amount+" where acc_no = "+acc_no;

        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            st.executeUpdate(sql_create_transaction_id);
            rs = st.executeQuery(sql_get_transaction_id);
            rs.next();
            int transaction_id = rs.getInt(1);
            st.executeUpdate("insert into all_transactions values(" + transaction_id + "," + withdraw_amount + ")");
            st.executeUpdate("insert into withdrawanddeposit values("+acc_no+", 'withdraw', "+transaction_id+")");
            st.executeUpdate(sql_update_balance);

            st.close();
            con.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return true;
    }

}
