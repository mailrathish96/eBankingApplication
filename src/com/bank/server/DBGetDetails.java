package com.bank.server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.*;

public class DBGetDetails {

    static ResultSet rs;
    static String s;
    static Statement st;
    static Connection con = null;
    static String sqlstr = "";

    public static void main(String[] argv) {

        System.out.println(get_transactions("2002","1"));
        //System.out.println(get_cheque_transactions("2001"));
        //System.out.println(get_withdraw_deposit_transactions("2001"));
        //bank("2001");
        //get_otherbank_transactions("2003");
        //transactions("2002", "1");
        /*JSONObject jsonObject = login("2001","1231");
        try {
            System.out.println(jsonObject.get("success").toString()+"  "+jsonObject.get("name").toString()+" "+jsonObject.get("balance"));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        //create("rathish");
    }

    public static int size(){
        String sqlstr = "SELECT * FROM users";
        int count = 0;
        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            rs = st.executeQuery(sqlstr);
            System.out.println(st);
            while(rs.next())
            {
              count++;
            }

            rs.close();
            st.close();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return count;
    }

    public static JSONObject login(String acc, String p_no){
        int acc_no = Integer.parseInt(acc);
        int pin = Integer.parseInt(p_no);
        JSONObject full_profile = new JSONObject();

        String sqlstr = "SELECT * FROM users WHERE acc_no = '"+acc_no+"'";
        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            rs = st.executeQuery(sqlstr);
            System.out.println(st);
            rs.next();
            System.out.println(rs.getString(1)+"  "+rs.getString(2)+"  "+rs.getInt(3)+" "+rs.getString(4));
            if(rs.getInt(3) == pin){
                full_profile.put("success", 1);
                full_profile.put("name", rs.getString(1));
                full_profile.put("acc_no", rs.getString(2));
                full_profile.put("balance", rs.getInt(4));
                full_profile.put("type", rs.getString(5));
            }
            else {
                full_profile.put("success", 0);
            }

            rs.close();
            st.close();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return full_profile;
    }

    public static JSONObject create(String name, String type){

        int acc_no = 2001+size();
        JSONObject jsonObject = new JSONObject();
        int pin_no = generatePIN();
        if(Integer.parseInt(type) == 1){
            sqlstr = "INSERT INTO users(username, acc_no, pin, balance, acc_type) VALUES('"+name+"',"+acc_no+","+pin_no+","+0+","+"'savings')";
        }
        else{
            sqlstr = "INSERT INTO users(username, acc_no, pin, balance, acc_type) VALUES('"+name+"',"+acc_no+","+pin_no+","+0+","+"'current')";
        }
        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            st.executeUpdate(sqlstr);
            jsonObject.put("success", 1);
            jsonObject.put("acc_no", acc_no);
            jsonObject.put("pin_no", pin_no);
            st.close();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public static JSONObject getFullProfile(String acc){
        int acc_no = Integer.parseInt(acc);
        JSONObject full_profile = new JSONObject();

        String sqlstr = "SELECT * FROM users WHERE acc_no = '"+acc_no+"'";
        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            rs = st.executeQuery(sqlstr);
            System.out.println(st);
            rs.next();
            System.out.println(rs.getString(1)+"  "+rs.getInt(2)+"  "+rs.getInt(3)+" "+rs.getString(4));

            full_profile.put("success", 1);
            full_profile.put("name", rs.getString(1));
            full_profile.put("acc_no", rs.getString(2));
            full_profile.put("balance", rs.getInt(4));
            full_profile.put("type", rs.getString(5));

            rs.close();
            st.close();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return full_profile;
    }

    public static int generatePIN(){
        int randomPIN = (int)(Math.random()*9000)+1000;
        return randomPIN;
    }

    public static JSONArray get_withdraw_deposit_transactions(String acc_no){
        JSONArray withdraw_deposit_json = new JSONArray();

        String sql_transaction ="select withdrawanddeposit.type, all_transactions.amount, transaction_id.timestamp, withdrawanddeposit.x_id from withdrawanddeposit" +
                "      inner join all_transactions on withdrawanddeposit.x_id = all_transactions.x_id" +
                "      inner join transaction_id on all_transactions.x_id = transaction_id.x_id where acc_no = '"+acc_no+"'";
        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            rs = st.executeQuery(sql_transaction);
            while(rs.next()){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("type", rs.getString(1));
                jsonObject.put("amount", rs.getInt(2));
                jsonObject.put("timestamp", rs.getString(3));
                jsonObject.put("transaction-id", rs.getInt(4));
                withdraw_deposit_json.put(jsonObject);
                System.out.println(rs.getString(1)+" "+rs.getInt(2)+" "+rs.getString(3)+" "+rs.getInt(4));
            }
            rs.close();
            st.close();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return withdraw_deposit_json;
    }

    public static JSONArray get_cheque_transactions(String acc_no){
        JSONArray withdraw_deposit_json = new JSONArray();

        String sql_transaction ="select cheque.acc_no, cheque.cheque_id, all_transactions.amount, transaction_id.timestamp, cheque.x_id from cheque\n" +
                "      inner join all_transactions on cheque.x_id = all_transactions.x_id\n" +
                "      inner join transaction_id on cheque.x_id = transaction_id.x_id where cheque.acc_no= '"+acc_no+"'";
        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            rs = st.executeQuery(sql_transaction);
            while(rs.next()){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("type", "check from "+rs.getInt(1)+" Checknumber-"+rs.getInt(2));
                jsonObject.put("amount", rs.getInt(3));
                jsonObject.put("timestamp", rs.getString(4));
                jsonObject.put("transaction-id", rs.getInt(5));
                withdraw_deposit_json.put(jsonObject);
                System.out.println("jdbc code:"+rs.getString(1)+" "+rs.getInt(2)+" "+rs.getString(3)+" "+rs.getString(4));
            }
            rs.close();
            st.close();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return withdraw_deposit_json;
    }

    public static JSONArray get_inbank_transactions(String acc_no){
        JSONArray withdraw_deposit_json = new JSONArray();

        String sql_transaction ="select inbank_acc_pairs.from_acc_no, inbank_acc_pairs.to_acc_no, all_transactions.amount, transaction_id.timestamp, inbank_transactions.x_id from inbank_acc_pairs \n" +
                "      inner join inbank_transactions on inbank_acc_pairs.a_a_id = inbank_transactions.a_a_id \n" +
                "      inner join all_transactions on inbank_transactions.x_id = all_transactions.x_id\n" +
                "      inner join transaction_id on all_transactions.x_id = transaction_id.x_id where inbank_acc_pairs.from_acc_no = '"+acc_no+"' or inbank_acc_pairs.to_acc_no = '"+acc_no+"'";
        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            rs = st.executeQuery(sql_transaction);
            while(rs.next()){
                JSONObject jsonObject = new JSONObject();
                if(rs.getString(1).equals(acc_no))
                    jsonObject.put("type", "fund transafer to "+rs.getString(2));
                else
                    if(rs.getString(2).equals(acc_no))
                        jsonObject.put("type","fund transafer from "+rs.getString(1));
                jsonObject.put("amount", rs.getInt(3));
                jsonObject.put("timestamp", rs.getString(4));
                jsonObject.put("transaction-id", rs.getInt(5));
                withdraw_deposit_json.put(jsonObject);
                System.out.println("jdbc code:"+rs.getString(1)+" "+rs.getInt(2)+" "+rs.getString(3)+" "+rs.getString(4));
            }
            rs.close();
            st.close();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return withdraw_deposit_json;
    }

    public static JSONArray get_otherbank_transactions(String acc_no){
        JSONArray withdraw_deposit_json = new JSONArray();

        String sql_transaction ="select accno_benid_pairs.acc_no, beneficiary.acc_no as beneficiary_accno, beneficiary.username, all_transactions.amount, transaction_id.timestamp, other_bank_transactions.x_id from accno_benid_pairs\n" +
                "      inner join beneficiary on accno_benid_pairs.ben_id = beneficiary.ben_id\n" +
                "      inner join other_bank_transactions on accno_benid_pairs.a_b_id = other_bank_transactions.a_b_id\n" +
                "      inner join all_transactions on all_transactions.x_id = other_bank_transactions.x_id\n" +
                "      inner join transaction_id on other_bank_transactions.x_id = transaction_id.x_id where accno_benid_pairs.acc_no = '"+acc_no+"'";
        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            rs = st.executeQuery(sql_transaction);
            while(rs.next()){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("type", "fund transafered to "+rs.getInt(2)+"("+rs.getString(3)+")");
                jsonObject.put("amount", rs.getInt(4));
                jsonObject.put("timestamp", rs.getString(5));
                jsonObject.put("transaction-id", rs.getInt(6));
                withdraw_deposit_json.put(jsonObject);
                System.out.println("jdbc code:"+rs.getString(1)+" "+rs.getInt(2)+" "+rs.getString(3)+" "+rs.getString(4));
            }
            rs.close();
            st.close();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return withdraw_deposit_json;
    }

    public static JSONArray get_transactions(String acc_no, String transaction_type){
        JSONArray jsonArray = new JSONArray();
        JSONArray all_transaction_array = new JSONArray();
        if(transaction_type.equals("1")){
            /*try {
                jsonArray = get_cheque_transactions(acc_no);
                for(int i=0;i<jsonArray.length();i++){
                    all_transaction_array.put(jsonArray.get(i));
                }
                jsonArray = get_withdraw_deposit_transactions(acc_no);
                for (int i=0;i<jsonArray.length();i++){
                    all_transaction_array.put(jsonArray.get(i));
                }
                jsonArray = get_inbank_transactions(acc_no);
                for (int i=0;i<jsonArray.length();i++){
                    all_transaction_array.put(jsonArray.get(i));
                }
                jsonArray = get_otherbank_transactions(acc_no);
                for (int i=0;i<jsonArray.length();i++){
                    all_transaction_array.put(jsonArray.get(i));
                }
                return all_transaction_array;
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
        }else if(transaction_type.equals("2"))
            return get_cheque_transactions(acc_no);
        else if(transaction_type.equals("3"))
            return get_withdraw_deposit_transactions(acc_no);
        else if(transaction_type.equals("4"))
            return get_inbank_transactions(acc_no);
        else if (transaction_type.equals("5"))
            return get_otherbank_transactions(acc_no);

        return jsonArray;
    }


}