package com.bank.server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.*;

/**
 * Created by rathi-pt1424 on 3/1/2017.
 */
public class DBAllTransactions {
    static ResultSet rs;
    static String s;
    static Statement st;
    static Connection con = null;


    public static void main(String[] argv) {

        System.out.println(get_all_transactions("2001", 5));

    }

    public static int size(){
        String sqlstr = "SELECT * FROM users";
        int count = 0;
        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            rs = st.executeQuery(sqlstr);
            System.out.println(st);
            while(rs.next())
            {
                count++;
            }

            rs.close();
            st.close();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return count;
    }

    public static JSONArray get_all_transactions(String acc_no, int row_no){

        String get_all_transaction =
                "select inbank_acc_pairs.from_acc_no as acc_no, concat('Fund transfer to ',inbank_acc_pairs.to_acc_no) as remarks, all_transactions.amount, transaction_id.timestamp, inbank_transactions.x_id from inbank_acc_pairs \n" +
                "      inner join inbank_transactions on inbank_acc_pairs.a_a_id = inbank_transactions.a_a_id\n" +
                "      inner join all_transactions on inbank_transactions.x_id = all_transactions.x_id\n" +
                "      inner join transaction_id on all_transactions.x_id = transaction_id.x_id where inbank_acc_pairs.from_acc_no = '"+acc_no+"'\n" +
                "union all\n" +
                "select inbank_acc_pairs.to_acc_no as acc_no, concat('Fund transfer From ',inbank_acc_pairs.from_acc_no) as remarks, all_transactions.amount, transaction_id.timestamp, inbank_transactions.x_id from inbank_acc_pairs \n" +
                "      inner join inbank_transactions on inbank_acc_pairs.a_a_id = inbank_transactions.a_a_id\n" +
                "      inner join all_transactions on inbank_transactions.x_id = all_transactions.x_id\n" +
                "      inner join transaction_id on all_transactions.x_id = transaction_id.x_id where inbank_acc_pairs.to_acc_no = '"+acc_no+"'\n" +
                "union all\n" +
                "select acc_no, withdrawanddeposit.type as remarks, all_transactions.amount, transaction_id.timestamp, withdrawanddeposit.x_id from withdrawanddeposit\n" +
                "      inner join all_transactions on withdrawanddeposit.x_id = all_transactions.x_id" +
                "      inner join transaction_id on all_transactions.x_id = transaction_id.x_id where acc_no = '"+acc_no+"'" +
                "union all\n" +
                "select accno_benid_pairs.acc_no, concat('Fund transfer to ',beneficiary.acc_no ,' - ', beneficiary.username,'(',beneficiary.bank_name,')') as remarks, all_transactions.amount, transaction_id.timestamp, other_bank_transactions.x_id from accno_benid_pairs\n" +
                "      inner join beneficiary on accno_benid_pairs.ben_id = beneficiary.ben_id\n" +
                "      inner join other_bank_transactions on accno_benid_pairs.a_b_id = other_bank_transactions.a_b_id\n" +
                "      inner join all_transactions on all_transactions.x_id = other_bank_transactions.x_id\n" +
                "      inner join transaction_id on other_bank_transactions.x_id = transaction_id.x_id where accno_benid_pairs.acc_no = '"+acc_no+"'\n" +
                "union all\n" +
                "select cheque.acc_no, concat('Cheque(',cheque.cheque_id,')') as remarks, all_transactions.amount, transaction_id.timestamp, cheque.x_id from cheque\n" +
                "      inner join all_transactions on cheque.x_id = all_transactions.x_id\n" +
                "      inner join transaction_id on cheque.x_id = transaction_id.x_id where cheque.acc_no = '"+acc_no+"'" +
                "ORDER by x_id\n" +
                "OFFSET "+row_no+" ROWS\n" +
                "FETCH NEXT 5 ROWS ONLY\n";


        JSONArray withdraw_deposit_json = new JSONArray();

        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/bank", "postgres", "password");
            st = con.createStatement();
            rs = st.executeQuery(get_all_transaction);
            while(rs.next()){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("type", rs.getString(2));
                jsonObject.put("amount", rs.getString(3));
                jsonObject.put("timestamp", rs.getString(4));
                jsonObject.put("transaction-id", rs.getString(5));
                withdraw_deposit_json.put(jsonObject);
                System.out.println("jdbc code:"+rs.getString(1)+" "+rs.getString(2)+" "+rs.getString(3)+" "+rs.getString(4));
            }
            rs.close();
            st.close();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return withdraw_deposit_json;
    }


}
