package com.bank.webclient;

import com.bank.server.DBInsert;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by rathi-pt1424 on 2/23/2017.
 */
public class DepositServlet extends HttpServlet {


    public void init() throws ServletException
    {
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int acc_no = Integer.parseInt(request.getParameter("acc_no"));
        int deposit_amount = Integer.parseInt(request.getParameter("deposit_amount"));
        response.setContentType("text/html");
        if(DBInsert.deposit_amount(acc_no,deposit_amount))
            response.getWriter().write("success");
        else
            response.getWriter().write("failure");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}
