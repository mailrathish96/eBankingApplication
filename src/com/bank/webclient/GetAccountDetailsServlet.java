package com.bank.webclient;

import com.bank.server.DBGetDetails;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by rathi-pt1424 on 2/22/2017.
 */
public class GetAccountDetailsServlet extends HttpServlet {

    public void init() throws ServletException
    {
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String acc_no = request.getParameter("acc_no");
        JSONObject jsonObject = DBGetDetails.getFullProfile(acc_no);

        try {
            if((int)jsonObject.get("success") == 1){
                response.setContentType("text/json");
                response.getWriter().write(String.valueOf(jsonObject));
            }
            else{
                response.setContentType("text/html");
                response.getWriter().write("failure");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}
