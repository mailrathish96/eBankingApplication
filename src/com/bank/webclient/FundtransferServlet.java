package com.bank.webclient;

import com.bank.server.DBTransactions;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by rathi-pt1424 on 2/24/2017.
 */
public class FundtransferServlet extends HttpServlet {
    public void init() throws ServletException
    {
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int acc_no = Integer.parseInt(request.getParameter("acc_no"));
        int to_acc_no = Integer.parseInt(request.getParameter("to_acc_no"));
        int transfer_amount = Integer.parseInt(request.getParameter("transfer_amount"));
        int type = Integer.parseInt(request.getParameter("type"));
        int pair_id = Integer.parseInt(request.getParameter("pair_id"));
        System.out.println(acc_no+" "+to_acc_no+" "+transfer_amount+" "+type+" "+pair_id);
        response.setContentType("text/html");
        if(DBTransactions.fund_transfer(acc_no,transfer_amount,pair_id,type,to_acc_no))
            response.getWriter().write("success");
        else
            response.getWriter().write("failure");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}
