package com.bank.webclient;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;

/**
 * Created by rathi-pt1424 on 2/28/2017.
 */
public class LoginFilter implements Filter {
    public void init(FilterConfig arg0) throws ServletException {}

    public void doFilter(ServletRequest req, ServletResponse resp,
                         FilterChain chain) throws IOException, ServletException {
        System.out.println(req.getParameter("acc_no"));
        if(req.getParameter("acc_no").toString().matches("[0-9][0-9][0-9][0-9]")){
            chain.doFilter(req, resp);
            System.out.println("filtering");
        }
        else
            System.out.println("filtering failed");
    }
    public void destroy() {}

}
