package com.bank.webclient;

import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by rathi-pt1424 on 2/28/2017.
 */
public class LogoutServlet extends HttpServlet {
    public void init() throws ServletException
    {
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getSession().invalidate();
        //response.sendRedirect(request.getContextPath() + "/index.jsp");
        System.out.println("servlet response");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}
