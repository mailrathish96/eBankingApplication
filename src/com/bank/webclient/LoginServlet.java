package com.bank.webclient;

import com.bank.server.DBGetDetails;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class LoginServlet extends HttpServlet {

    public void init() throws ServletException
    {
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String acc_no = request.getParameter("acc_no");
        String pin_no = request.getParameter("pin_no");
        JSONObject jsonObject = DBGetDetails.login(acc_no, pin_no);

        try {
            if((int)jsonObject.get("success") == 1){
                HttpSession session =  request.getSession();
                session.setAttribute("acc_no",acc_no);
                session.setAttribute("login-status", "login");
                response.setContentType("text/html");
                response.getWriter().write("success");
            }
            else{
                response.setContentType("text/html");
                response.getWriter().write("failure");
                /*request.setAttribute("message", "faliure");
                RequestDispatcher view=request.getRequestDispatcher("wrongcredentials.jsp");
                view.forward(request,response);*/
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}
