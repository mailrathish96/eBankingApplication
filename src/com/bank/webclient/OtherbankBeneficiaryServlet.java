package com.bank.webclient;

import com.bank.server.DBInsert;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by rathi-pt1424 on 2/24/2017.
 */
public class OtherbankBeneficiaryServlet extends HttpServlet {

    public void init() throws ServletException
    {
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int acc_no = Integer.parseInt(request.getParameter("acc_no"));
        int from_acc_no = Integer.parseInt(request.getParameter("from_acc_no"));
        String name = request.getParameter("name");
        String bank_name = request.getParameter("bank_name");
        String branch_name = request.getParameter("branch_name");
        String ifsc = request.getParameter("ifsc");
        //System.out.println(from_acc_no+" "+acc_no+" "+name+" "+bank_name+" "+branch_name+" "+ifsc);
        DBInsert.add_otherbank_beneficiary(from_acc_no, acc_no, name, bank_name, branch_name, ifsc);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}
