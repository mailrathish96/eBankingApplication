package com.bank.webclient;

import com.bank.server.DBGetDetails;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by rathi-pt1424 on 3/1/2017.
 */
public class SignupServlet extends HttpServlet {

    public void init() throws ServletException
    {
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String type = request.getParameter("type");
        JSONObject jsonObject = DBGetDetails.create(name, type);

        try {
            if((int)jsonObject.get("success") == 1){
                response.setContentType("text/html");
                System.out.println(jsonObject.get("acc_no"));
                response.getWriter().write(String.valueOf(jsonObject));
            }
            else{
                response.setContentType("text/html");
                response.getWriter().write("failure");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}
