package com.bank.webclient;

import com.bank.server.DBTransactions;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by rathi-pt1424 on 2/24/2017.
 */
public class GetBeneficiaryListServlet extends HttpServlet {
    public void init() throws ServletException
    {
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String acc_no = request.getParameter("acc_no");
        JSONArray jsonArray = DBTransactions.get_beneficiary_list(acc_no);
        response.setContentType("text/json");
        System.out.println("ServletCopde:"+jsonArray);
        response.getWriter().write(String.valueOf(jsonArray));
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}
