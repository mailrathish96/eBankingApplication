package com.bank.webclient;

import com.bank.server.DBAllTransactions;
import com.bank.server.DBGetDetails;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by rathi-pt1424 on 2/22/2017.
 */
public class TransactionHistoryServlet extends HttpServlet {

    JSONArray jsonArray;
    public void init() throws ServletException
    {
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String acc_no = request.getParameter("acc_no");
        String transaction_type = request.getParameter("transaction_type");
        String row_no = request.getParameter("row_no");
        System.out.println(acc_no+"    "+transaction_type);
        if(Integer.parseInt(transaction_type) != 1 ){
            jsonArray = DBGetDetails.get_transactions(acc_no,transaction_type);
        }
        else{
            jsonArray = DBAllTransactions.get_all_transactions(acc_no, Integer.parseInt(row_no));
        }
        System.out.println("servlet code :"+jsonArray);
        response.setContentType("text/json");
        response.getWriter().write(String.valueOf(jsonArray));
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}
