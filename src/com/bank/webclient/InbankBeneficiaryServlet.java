package com.bank.webclient;

import com.bank.server.DBInsert;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by rathi-pt1424 on 2/23/2017.
 */
public class InbankBeneficiaryServlet extends HttpServlet {

    public void init() throws ServletException
    {
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int acc_no = Integer.parseInt(request.getParameter("acc_no"));
        int to_acc_no = Integer.parseInt(request.getParameter("to_acc_no"));
        DBInsert.add_inbank_beneficiary(acc_no,to_acc_no);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}
